import logic as logic
import time

if __name__ == '__main__':
    while True:
        key = input('action: ')
        if key == 'a':
            action = logic.Action(True)
        else:
            action = logic.Action(False)
        game_state = logic.next_tick(action)
        print(game_state)
        time.sleep(0.5)
