import os
import sys

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from logic.data_classes import GameState, Action, Cactus
from logic.main import next_tick, reset
from logic.replay_memory import TickObject, get_random_samples, build_tick_object_from_dict, \
    get_action, tick_object_to_game_states, save_entry_metrics, MetricsObject

BATCHSIZE = 50
PENALTY = -1.0
learningRate = 1e-3
lossFunction = keras.losses.Huber()
optimizer = keras.optimizers.Adam(learning_rate=learningRate, clipnorm=1.0)
lastState: GameState = next_tick(Action(False))


def create_q_model():
    # Network defined by the Deepmind paper
    #                           dino+cacti*(xy)+speed
    inputs = layers.Input(shape=((2 + 4 * 2 + 1),))

    layer0 = layers.Flatten()(inputs)

    # Convolutions on the frames on the screen
    layer1 = layers.Dense(32, activation="relu")(layer0)
    layer2 = layers.Dense(16, activation="relu")(layer1)
    layer3 = layers.Dense(8, activation="relu")(layer2)
    action = layers.Dense(2, activation="linear")(layer3)

    return keras.Model(inputs=inputs, outputs=action)


model = create_q_model()
modelTarget = create_q_model()


def build_game_state_array(state: GameState) -> np.ndarray:
    array = [lastState.dino.speed_y, lastState.dino.y, state.speed]
    cactus: Cactus
    for cactus in lastState.cacti:
        array.append(cactus.x)
        array.append(cactus.y)

    return np.array(array)


# Werte von handgecodeter Logik
# Input Values 0-1 ausprobieren
# Nur 1 Kaktus
# Reward problematisch, bei Sprung noch kein Reward

# penalty springen
#

def main():
    global lastState, modelTarget, model
    i = 0
    while True:
        print(f"Training game id: {lastState.game_id}")
        while not lastState.dead:
            last_array = build_game_state_array(lastState)
            state_tensor = tf.convert_to_tensor(last_array)
            state_tensor = tf.expand_dims(state_tensor, 0)
            action_probs = model(state_tensor, training=False)
            action: int = int(tf.argmax(action_probs[0]).numpy())
            action: Action = Action(action == 1)
            new_last_state = next_tick(action)
            train(lastState, new_last_state, action)
            lastState = new_last_state

        print("Lost! Restarting!")
        print(f"Highscore: {lastState.high_score}")
        save_entry_metrics(MetricsObject(lastState.game_id, lastState.high_score, 1, i))
        modelTarget.set_weights(model.get_weights())
        modelTarget.save_weights(
            os.path.join("data/model", f"model-pixel_{i}_{lastState.game_id}_{lastState.high_score:.4f}.h5"))
        reset()
        lastState = next_tick(Action(False))
        i += 1


def get_x(batch, state: int):
    assert state == 0 or state == 1  # 0 is currentstate, 1 is future state
    x = [item[state] for item in batch]

    return np.array(x, dtype=np.float32)


def backprop(batch):
    rewards_sample = [batch[i][3] for i in range(len(batch))]
    action_sample = [batch[i][2] for i in range(len(batch))]
    done_sample = tf.convert_to_tensor([float(batch[i][4]) for i in range(len(batch))])

    x = get_x(batch, 0)
    x_target = get_x(batch, 1)

    future_rewards = modelTarget.predict(x_target)

    updated_q_values = rewards_sample + 0.99 * tf.reduce_max(future_rewards, axis=1)
    updated_q_values = updated_q_values * (1 - done_sample) - done_sample * abs(PENALTY)

    masks = tf.one_hot(action_sample, 2)

    with tf.GradientTape() as tape:

        # Train the model on the states and updated Q-values
        q_values = model(x)

        # Apply the masks to the Q-values to get the Q-value for action taken
        q_action = tf.reduce_sum(tf.multiply(q_values, masks), axis=1)
        # Calculate loss between new Q-value and old Q-value
        loss = lossFunction(updated_q_values, q_action)

        # if self.debugcounter % 20 == 0:
        #    print(self.debugcounter)
        #    print(updated_q_values)
        #    print(rewards_sample)
        #    print(q_action)
        # self.debugcounter += 1

    grads = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(grads, model.trainable_variables))


def build_train_array(last_state: GameState, next_state: GameState, action: Action):
    last_state_array = build_game_state_array(last_state)
    next_state_array = build_game_state_array(next_state)
    # action_array = np.array([action.jump])
    # reward_array = np.array([next_state.high_score])
    return [last_state_array, next_state_array, action.jump, next_state.high_score - last_state.high_score, next_state.dead]


def get_replay_memory_entries(sample_size: int) -> []:
    random_samples = get_random_samples(sample_size)
    memory_entries = []
    sample: dict
    for sample in random_samples:
        sample: TickObject = build_tick_object_from_dict(sample)
        last_state: GameState
        next_state: GameState
        (last_state, next_state) = tick_object_to_game_states(sample)

        train_array = build_train_array(
            last_state,
            next_state,
            get_action(sample.action)
        )

        memory_entries.append(train_array)

    return memory_entries


def train(last_state: GameState, next_state: GameState, action: Action):
    # https://pythonprogramming.net/training-deep-q-learning-dqn-reinforcement-learning-python-tutorial/

    # currentstate = "current_{}_{}.png".format(i,j)

    # nextstate = "next_{}_{}.png".format(i,j)

    # self.checkshufflelist("in train before pop_batch")

    batch = get_replay_memory_entries(BATCHSIZE)

    batch.append(build_train_array(last_state, next_state, action))
    batch = np.array(batch)
    backprop(batch)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        model_name = sys.argv[1]
        path = os.path.join("data/model", model_name)
        modelTarget.load_weights(path)
        model.load_weights(path)

    main()
