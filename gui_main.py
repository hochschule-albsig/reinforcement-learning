import os
import sys

import pygame

import logic.main as logic
from logic.data_classes import Action, GameState
from logic.main import CACTUS_HEIGHT
from logic.replay_memory import get_game

FPS = 60
JUMP_SCALING = 1

pygame.init()

pygame.font.init()
myfont = pygame.font.SysFont('Arial Black', 15)

scr_size = (width, height) = (600, 150)

gameStart = False
game_over = False

high_score = 0
current_score = 0

black = (0, 0, 0)
white = (255, 255, 255)
background_col = (235, 235, 235)

screen = pygame.display.set_mode(scr_size)
clock = pygame.time.Clock()
pygame.display.set_caption("Dino Run")


def load_sprite_sheet(
        sheetname
):
    fullname = os.path.join("sprites", sheetname)
    sheet = pygame.image.load(fullname)

    if (sheetname == "dion_single.png"):
        sheet = pygame.transform.scale(sheet, (55, 55))  # Scale of Dino
    else:
        sheet = pygame.transform.scale(sheet, (CACTUS_HEIGHT, CACTUS_HEIGHT))  # Scale of Cacti

    sheet_rect = sheet.get_rect()

    return sheet, sheet_rect


class Dino():
    def __init__(self, sizex=-1, sizey=-1):
        self.image, self.rect = load_sprite_sheet('dion_single.png')
        self.rect.bottom = int(0.98 * height)
        self.rect.left = width / 15
        self.index = 0
        self.counter = 0
        self.score = 0
        self.isJumping = False
        self.isDead = False

        self.stand_pos_width = self.rect.width

    def draw(self):
        screen.blit(self.image, self.rect)


class Cactus(pygame.sprite.Sprite):
    def __init__(self, x, y):
        self.image, self.rect = load_sprite_sheet("cacti.png")
        self.rect.bottom = int(0.98 * height)
        self.rect.left = (width / 15) + x

    def draw(self):
        screen.blit(self.image, self.rect)

    def update(self, x):
        self.rect.left = (width / 15) + x


def update_screen():
    screen.fill(background_col)
    background_image = pygame.image.load("sprites/Boden.png")
    screen.blit(background_image, [0, 0])

    temp_dino.draw()

    pygame.display.update()


def introscreen():
    global temp_dino, cacti

    cacti = []
    temp_dino = Dino(44, 47)
    gameStart = False

    while not gameStart:
        if pygame.display.get_surface() == None:
            print("Couldn't load display surface")
            return True
        else:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE or event.key == pygame.K_UP:
                        gameStart = True

                        gameState = next_tick(Action(False))
                        print("Jump", gameState)
                        for c in gameState.cacti:
                            cacti.append(Cactus(c.y, c.y))
                        temp_dino.rect.bottom = (int(0.98 * height)) - gameState.dino.y

        if pygame.display.get_surface() != None:
            update_screen()

        clock.tick(FPS)


def gameplay():
    global high_score
    while True:
        if pygame.display.get_surface() == None:
            print("Couldn't load display surface")
            return True
        else:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE or event.key == pygame.K_UP:
                        gameState = next_tick(Action(True))
                        print("Jump", gameState)
                        temp_dino.rect.bottom = (int(0.98 * height)) - gameState.dino.y
                        temp_dino.isJumping = True

        if temp_dino.isJumping == False:
            gameState = next_tick(Action(False))
            print("run", gameState)
            temp_dino.rect.bottom = (int(0.98 * height)) - gameState.dino.y

        temp_dino.isJumping = False

        if gameState.dead == True:
            while True:
                for event_dead in pygame.event.get():
                    if event_dead.type == pygame.QUIT:
                        return True
                    if event_dead.type == pygame.KEYDOWN:
                        if event_dead.key == pygame.K_SPACE or event_dead.key == pygame.K_UP:
                            logic.reset()
                            introscreen()
                            gameplay()

        current_score = int(gameState.high_score)

        if (current_score > high_score):
            high_score = current_score

        screen.fill(background_col)
        background_image = pygame.image.load("sprites/Boden.png")

        zero_filled_highscore = str(high_score).zfill(5)  # add leading zeros to highscore
        zero_filled_current_score = str(current_score).zfill(5)  # add leading zeros to currentscore

        score_display = myfont.render('HI: ' + zero_filled_highscore + ' ' + zero_filled_current_score, False,
                                      (0, 0, 0))  # display highscore and current score

        screen.blit(background_image, [0, 0])  # draw background

        temp_dino.draw()  # draw dino

        i = 0
        for c in cacti:  # draw cacti
            c.update(gameState.cacti[i].x)
            c.draw()
            i = i + 1

        screen.blit(score_display, (450, 0))
        pygame.display.update()

        clock.tick(FPS)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE or event.key == pygame.K_UP:
                gameplay()


replay: [GameState] = []
replayIndex = 0


def next_tick(action: Action) -> GameState:
    global replay, replayIndex
    if len(replay) > 0:
        # TODO: Enhancement check for new entries async and load them
        replayIndex += 1
        if replayIndex < len(replay):
            return replay[replayIndex]
        else:
            return replay[-1]

    else:
        return logic.next_tick(action)


def main():
    isGameQuit = introscreen()
    if not isGameQuit:
        gameplay()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        replayId = sys.argv[1]
        replay = get_game(replayId)

    main()
