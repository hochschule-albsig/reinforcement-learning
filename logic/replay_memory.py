import copy

from pymongo import MongoClient
from pymongo import WriteConcern

from logic.data_classes import Action, GameState
from logic.data_classes import Cactus
from logic.data_classes import Dino


class TickObject:
    def __init__(self, game_id: str, tick_count: int, action: Action, last: GameState, next: GameState):
        self.game_id = game_id
        self.tick_count = tick_count
        self.action = action.__dict__

        self.last_speed = last.speed
        self.last_cacti = [cactus.__dict__ for cactus in last.cacti]
        self.last_dino = last.dino.__dict__
        self.last_high_score = last.high_score
        self.last_dead = last.dead
        self.next_speed = next.speed
        self.next_cacti = [cactus.__dict__ for cactus in next.cacti]
        self.next_dino = next.dino.__dict__
        self.next_high_score = next.high_score
        self.next_dead = next.dead


class MetricsObject:
    def __init__(self, game_id: str, high_score: float, time_p_tick: int, game_number: int):
        self.game_id = game_id
        self.high_score = high_score
        self.time_p_tick = time_p_tick
        self.game_number = game_number


def get_cacti(cacti_dict: [dict]) -> [Cactus]:
    return [Cactus(cactus["x"], cactus["y"]) for cactus in cacti_dict]


def get_dino(dino_dict: dict) -> Dino:
    return Dino(dino_dict["y"])


def get_action(action_dict: dict) -> Action:
    return Action(action_dict["jump"])


def build_tick_object_from_dict(tick_object_dict: dict) -> TickObject:
    return TickObject(
        tick_object_dict['game_id'],
        tick_object_dict['tick_count'],
        get_action(tick_object_dict["action"]),
        GameState(
            tick_object_dict['game_id'],
            get_cacti(tick_object_dict["last_cacti"]),
            get_dino(tick_object_dict["last_dino"]),
            tick_object_dict['last_dead'],
            tick_object_dict['last_high_score'],
            tick_object_dict['last_speed']
        ),
        GameState(
            tick_object_dict['game_id'],
            get_cacti(tick_object_dict["next_cacti"]),
            get_dino(tick_object_dict["next_dino"]),
            tick_object_dict['next_dead'],
            tick_object_dict['next_high_score'],
            tick_object_dict['next_speed']
        )
    )

def open_database():
    client = MongoClient('localhost', 27017)
    db = client.reinforcement_learning
    #collection = db.replay_memory.with_options(write_concern=WriteConcern(w=0))
    return db


collection = open_database().replay_memory.with_options(write_concern=WriteConcern(w=0))
collection_metrics = open_database().metrics.with_options(write_concern=WriteConcern(w=0))
cache: [TickObject] = []
cache_entries = 100


def save_entry(tick_object: TickObject):
    global cache, cache_entries
    cache.append(copy.deepcopy(tick_object).__dict__)
    if len(cache) >= cache_entries:
        print("Flushing to mongo")
        collection.insert_many(cache)
        cache = []

def save_entry_metrics(metrics_object: MetricsObject):
    collection_metrics.insert_one(metrics_object.__dict__)


def get_random_samples(sample_size: int):
    return collection.aggregate([{"$sample": {"size": sample_size}}])


def tick_object_to_game_states(tick_object: TickObject) -> (GameState, GameState):
    return (GameState(tick_object.game_id,
                      get_cacti(tick_object.last_cacti),
                      get_dino(tick_object.last_dino),
                      tick_object.last_dead,
                      tick_object.last_high_score,
                      tick_object.last_speed),

            GameState(tick_object.game_id,
                      get_cacti(tick_object.next_cacti),
                      get_dino(tick_object.next_dino),
                      tick_object.next_dead,
                      tick_object.next_high_score,
                      tick_object.next_speed))


def get_game(game_id: str) -> [GameState]:
    games_dict = collection.find({"game_id": game_id}).sort("tickCount")
    tick_objects = [build_tick_object_from_dict(tick) for tick in games_dict]
    game_states = [tick_object_to_game_states(tick)[0] for tick in tick_objects]
    return game_states
