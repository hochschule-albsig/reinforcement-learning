JUMP_ACCELERATION = 6.5
EARTH_ACCELERATION = 0.3


class Cactus:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def next_tick(self, speed: float):
        self.x = self.x - speed

    def __str__(self):
        return "x: " + str(self.x) + " y: " + str(self.y)


class Dino:
    def __init__(self, y: int = 0):
        self.y = y
        self.speed_y = 0

    def jump(self):
        if self.speed_y <= 0.1 and self.y < 1:
            self.speed_y = JUMP_ACCELERATION

    def next_tick(self):
        if self.y >= 0:
            self.y = self.y + self.speed_y
            self.speed_y = self.speed_y - EARTH_ACCELERATION

        if self.y <= 0:
            self.y = 0
            self.speed_y = 0

    def __str__(self):
        return "y: " + str(self.y) + " speed: " + str(self.speed_y)


class GameState:
    def __init__(self, game_id: str, cacti: [Cactus], dino: Dino, dead: bool, high_score: float, speed: float):
        self.game_id = game_id
        self.cacti = cacti
        self.dino = dino
        self.dead = dead
        self.high_score = high_score
        self.speed = speed

    def __str__(self):
        retStr = ""
        for cactus in self.cacti:
            retStr += "Cactus: " + str(cactus) + " "

        return retStr + " dino: " + str(self.dino) + " dead: " + str(self.dead) + " score: " + str(self.high_score)


class Action:
    def __init__(self, jump: bool):
        self.jump = jump
