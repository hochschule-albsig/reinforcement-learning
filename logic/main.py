import copy
import math
import uuid
from random import randint

from logic.data_classes import Action
from logic.data_classes import Cactus
from logic.data_classes import Dino
from logic.data_classes import GameState
from logic.replay_memory import save_entry, TickObject, save_entry_metrics, MetricsObject

INITIAL_SPEED = 5
SPEED_INCREASE = 0.001
CACTI_COUNT = 4
CACTI_VISIBLE = 2
GAME_SIZE = 500
CACTUS_HEIGHT = 40
CACTUS_WIDTH = 30
CACTUS_MIN_DISTANCE = GAME_SIZE / 2


def handle_dino(action: Action):
    if action.jump:
        dino.jump()

    dino.next_tick()


def remove_cacti_if_not_visible():
    cactus: Cactus
    for cactus in cacti:
        if cactus.x + CACTUS_WIDTH < 0:
            cacti.remove(cactus)


def generate_new_cacti_if_necessary(cacti: [Cactus]) -> [Cactus]:
    while len(cacti) < CACTI_COUNT:
        if len(cacti) <= 0:
            last_cactus_x = GAME_SIZE
        else:
            last_cactus_x = cacti[-1].x

        new_cactus_x = last_cactus_x + randint(int(CACTUS_MIN_DISTANCE), int(GAME_SIZE))

        cacti.append(Cactus(x=new_cactus_x, y=CACTUS_HEIGHT))

    return cacti


def handle_cacti(speed: float):
    global cacti
    cactus: Cactus
    for cactus in cacti:
        cactus.next_tick(speed)

    remove_cacti_if_not_visible()
    cacti = generate_new_cacti_if_necessary(cacti)


def check_for_cactus_dino_overlap(cactus: Cactus) -> bool:
    return (cactus.x + CACTUS_WIDTH) > 0 and cactus.x < CACTUS_WIDTH


def check_for_collision(dino: Dino, cactus: Cactus) -> bool:
    if check_for_cactus_dino_overlap(cactus):
        if dino.y < CACTUS_HEIGHT:
            return True

    return False


def reset():
    global speed, cacti, dino, dead, game_id, tickCount, highScore
    speed = INITIAL_SPEED
    cacti = generate_new_cacti_if_necessary([])
    dino = Dino()
    dead = False
    game_id = str(uuid.uuid4())
    tickCount = 0
    highScore = 0


speed: int = INITIAL_SPEED
cacti: [Cactus] = generate_new_cacti_if_necessary([])
dino: Dino = Dino()
dead: bool = False
highScore: int = 0
game_id: str = str(uuid.uuid4())
tickCount: int = 0


def calculate_new_highscore(current_highscore: int, dead: bool, new_first_cactus: Cactus,
                            old_first_cactus: Cactus, action) -> int:
    global speed
    if dead:
        return current_highscore

    return current_highscore + 1


def next_tick(action: Action) -> GameState:
    global speed, cacti, dino, dead, highScore, game_id, tickCount

    last_game_state = GameState(game_id, copy.deepcopy(cacti), copy.deepcopy(dino), dead, highScore, speed)

    tickCount += 1
    # This is intentionally just a reference, see logic in calculate_new_highscore, to see why, in short,
    # it checks if a cactus was removed, by comparing the references
    old_first_cactus = cacti[0]

    if not dead:
        speed = speed + SPEED_INCREASE
        handle_dino(action)
        handle_cacti(speed)

        if len(cacti) > 0:
            dead = check_for_collision(dino, cacti[0])

    highScore = calculate_new_highscore(highScore, dead, cacti[0], old_first_cactus, action)

    new_game_state = GameState(game_id, cacti, dino, dead, highScore, speed)

    save_entry(TickObject(game_id, tickCount, action, last_game_state, new_game_state))

    return new_game_state
